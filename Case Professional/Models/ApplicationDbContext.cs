﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Case_Professional.Models
{
    public interface IApplicationDbContext
    {
        int SaveChanges();
        IDbSet<Organisation> Organisations { get; set; }
    }

    public class FakeApplicationDbContext : IApplicationDbContext
    {
        public int SaveChanges()
        {
            return 0;
        }

        public IDbSet<Organisation> Organisations { get; set; }
    }
    
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public IDbSet<Organisation> Organisations { get; set; }
    }
}