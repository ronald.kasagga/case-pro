﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Case_Professional.Models
{
    public class Organisation
    {
        public int Id { get; set; }

        [Required]
        [RegularExpression(@"^[A-za-z0-9 ]+$", ErrorMessage = "Please type a valid Organisation Name. only letters and digits are permitted")]
        [Display(Name = "Organisation Name")]
        public string Name { get; set; }
        
        [Display(Name = "License Key")]
        public string LicenseKey { get; set; }
        
        [Display(Name = "Created On")]
        public DateTime AddedOn { get; set; }
        
        [Display(Name = "Updated On")]
        public DateTime UpdatedOn { get; set; }

    }
}