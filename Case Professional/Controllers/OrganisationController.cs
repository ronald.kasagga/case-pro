﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Case_Professional.Models;
using Case_Professional.Services;

namespace Case_Professional.Controllers
{
    [Authorize(Roles = "Admin")]
    public class OrganisationController : Controller
    {
        
        private readonly IApplicationDbContext _db;

        public OrganisationController()
        {
            _db = new ApplicationDbContext();
        }

        public OrganisationController(IApplicationDbContext dbContext)
        {
            _db = dbContext;
        }

        //
        // GET: /Organisation/
        public ActionResult Index()
        {
            return View(_db.Organisations);
        }

        //
        // GET: /Organisation/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Organisation/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Organisation/Create
        [HttpPost]
        public ActionResult Create(Organisation organisation)
        {
            organisation.AddedOn = DateTime.UtcNow;
            organisation.UpdatedOn = DateTime.UtcNow;
            try
            {
                _db.Organisations.Add(organisation);
                _db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Organisation/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Organisation/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Organisation/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Organisation/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult ResetLicenseKey(int id)
        {
            new OrganisationService(_db)
                .ResetOrganisationLicenseKey(id);
            return Content(_db.Organisations.Find(id).LicenseKey);
        }
    }
}
