using System.Collections.Generic;
using Case_Professional.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Case_Professional.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Case_Professional.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Case_Professional.Models.ApplicationDbContext context)
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var admins = new List<string> {"ronald.kasagga", "andrew.olobo", "douglas.were"};
            const string adminRole = "Admin";

            foreach (var username in admins)
            {
                if (context.Users.Any(t => t.UserName == username)) 
                    continue;

                var user = new ApplicationUser
                {
                    UserName = username,
                    Email = String.Format("{0}@gmail.com", username)
                };

                context.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = adminRole });
                context.SaveChanges();

                userManager.Create(user, "passW0rd!");
                userManager.AddToRole(user.Id, adminRole);
            }

            
            //  This method will be called after migrating to the latest version. 

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
