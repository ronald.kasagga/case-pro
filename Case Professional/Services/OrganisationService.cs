﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Case_Professional.Models;

namespace Case_Professional.Services
{
    public class OrganisationService
    {
        private readonly IApplicationDbContext _db;

        public OrganisationService(IApplicationDbContext db)
        {
            _db = db;
        }

        public void ResetOrganisationLicenseKey(int organisationId)
        {
            var organisation = _db.Organisations.Find(organisationId);
            organisation.LicenseKey = Guid.NewGuid().ToString().ToUpper();
            _db.SaveChanges();
        }
    }
}