﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Case_Professional.Startup))]
namespace Case_Professional
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
